import datetime

from django.test import TestCase

from hoodunite.apps.authentication.models import User


class TestUserModel(TestCase):
    def setUp(self):
        self.adult = User.objects.create_user(username='user1',
                                              password='password1', date_of_birth=datetime.datetime(1999, 3, 29))
        self.teenager = User.objects.create_user(username='user2',
                                                 password='password2', date_of_birth=datetime.datetime(2003, 3, 22))

    def test_is_adult(self):
        self.assertEqual(self.adult.is_adult, True)
        self.assertEqual(self.teenager.is_adult, False)
