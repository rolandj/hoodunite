from unittest import mock

from django.test import TestCase

from rest_framework.exceptions import PermissionDenied

from hoodunite.apps.authentication.models import User
from hoodunite.apps.neighborhoods.models import Neighborhood
from hoodunite.apps.authentication.selectors import get_user, get_users


class TestSelectors(TestCase):
    def setUp(self):
        self.data = {
            'name': 'Test Name',
            'description': 'Lorem ipsum test doloret',
            'price': 100
        }

        self.neighborhood = Neighborhood.objects.create(name='TestNeighborhood')
        self.second_neighborhood = Neighborhood.objects.create(name='TestNeighborhood2')

        self.user = User.objects.create(username='TestUser', password='TestPassword', neighborhood=self.neighborhood)
        self.second_user = User.objects.create(username='TestUser2', password='TestPassword',
                                               neighborhood=self.second_neighborhood)

    @mock.patch('hoodunite.apps.authentication.selectors.User.objects.filter')
    def test_get_users_return_only_users_from_same_neighborhood(self, get_users_mock):
        get_users(self.user)
        get_users_mock.assert_called_with(neighborhood__id=self.user.neighborhood.id)

    def test_get_user_cant_return_user_from_other_neighborhood(self):
        self.assertRaises(PermissionDenied, lambda: get_user(self.user, self.second_user.id))
