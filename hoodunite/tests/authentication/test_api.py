from unittest import mock

from django.test import TestCase

from rest_framework.test import APIClient

from hoodunite.apps.authentication.models import User


class TestUserList(TestCase):
    """Test UserList API against calling demanded services / selectors."""

    def setUp(self):
        self.client = APIClient()
        self.url = '/api/users/'
        self.user = User.objects.create(username='TestUser', password='TestPassword', email='test@test.com')

    @mock.patch('hoodunite.apps.authentication.api.get_users')
    def test_get_calls_selector(self, get_user_list_mock):
        self.client.force_authenticate(self.user)

        self.client.get(self.url)
        get_user_list_mock.assert_called_once()

    def test_user_signs_up_and_gets_tokens(self):
        user_data = {
            'username': 'TestUsername',
            'password': 'TestPassword',
            'email': 'test@email.com',
        }
        response = self.client.post(self.url, data=user_data)

        self.assertEqual(response.status_code, 201)
        assert type(response.data.get('tokens')) is dict


class TestUserDetail(TestCase):
    """Test UserDetail API against calling demanded services / selectors."""

    def setUp(self):
        self.client = APIClient()
        self.url = '/api/users/'
        self.user = User.objects.create(username='TestUser', password='TestPassword', email='test@test.com')
        self.client.force_authenticate(self.user)

    @mock.patch('hoodunite.apps.authentication.api.get_user')
    def test_get_calls_selector(self, get_user_mock):
        get_user_mock.return_value = self.user

        self.client.get(f'{self.url}{self.user.id}/')
        get_user_mock.assert_called_once_with(self.user, self.user.id)

    @mock.patch('hoodunite.apps.authentication.api.update_user')
    def test_put_calls_service(self, update_user_mock):
        user_data = {
            'username': 'TestUsername',
            'password': 'TestPassword2',
            'email': 'test@email.com',
        }
        self.client.put(f'{self.url}{self.user.id}/', data=user_data, format='json')
        update_user_mock.assert_called_once_with(self.user, self.user.id, **user_data)

    @mock.patch('hoodunite.apps.authentication.api.patch_user')
    def test_patch_calls_service(self, patch_user_mock):
        user_data = {
            'email': 'test@email.com'
        }
        self.client.patch(f'{self.url}{self.user.id}/', data=user_data, format='json')
        patch_user_mock.assert_called_once_with(self.user, self.user.id, **user_data)

    @mock.patch('hoodunite.apps.authentication.api.delete_user')
    def test_delete_calls_service(self, delete_user_mock):
        self.client.delete(f'{self.url}{self.user.id}/')
        delete_user_mock.assert_called_once_with(self.user, self.user.id)
