from unittest import mock

from django.test import TestCase

from rest_framework.exceptions import PermissionDenied

from hoodunite.apps.location.models import Address
from hoodunite.apps.authentication.models import User
from hoodunite.apps.neighborhoods.models import Neighborhood
from hoodunite.apps.neighborhoods.selectors import get_neighborhoods, get_instance, check_permissions


class TestSelectors(TestCase):
    def setUp(self):
        self.data = {
            "country": "TestCountry",
            "province": "TestProvince",
            "city": "TestCity",
            "street": "TestStreet",
            "number": 1,
            "longitude": 1,
            "latitude": 1,
        }
        self.address = Address.objects.create(**self.data)
        self.second_address = Address.objects.create(**self.data)
        self.neighborhood = Neighborhood.objects.create(name='TestHood', address=self.address)
        self.second_neighborhood = Neighborhood.objects.create(name='TestHood2', address=self.second_address)
        self.user = User.objects.create(username='TestUser',
                                        password='TestPassword',
                                        neighborhood=self.neighborhood,
                                        address=self.address)
        self.second_user = User.objects.create(username='TestUser2',
                                               password='TestPassword',
                                               neighborhood=self.second_neighborhood,
                                               address=self.second_address)

    @mock.patch('hoodunite.apps.neighborhoods.selectors.Neighborhood.objects.filter')
    def test_neighborhood_filtering_with_query_params(self, hood_filter_mock):
        params_country = {'country': ["TestCountry"]}
        params_province = {'province': ["TestProvince"]}
        params_city = {'city': ["TestCity"]}

        get_neighborhoods(self.user, **params_country)
        hood_filter_mock.assert_called_with(address__country=params_country.get('country')[0])

        get_neighborhoods(self.user, **params_province)
        hood_filter_mock.assert_called_with(address__province=params_province.get('province')[0])

        get_neighborhoods(self.user, **params_city)
        hood_filter_mock.assert_called_with(address__city=params_city.get('city')[0])

    def test_permissions(self):
        self.assertEqual(check_permissions(self.user, self.neighborhood), True)
        self.assertRaises(PermissionDenied, lambda: check_permissions(self.user, self.second_neighborhood))

    def test_user_cannot_edit_neighborhood_when_there_are_more_users(self):
        self.user.neighborhood = self.second_neighborhood
        self.user.save()
        self.assertRaises(PermissionDenied, lambda: check_permissions(self.user, self.second_neighborhood))

    def test_get_instance(self):
        self.assertEqual(get_instance(hood_id=self.neighborhood.id), self.neighborhood)
        self.assertEqual(get_instance(hood_name=self.neighborhood.name), self.neighborhood)
        self.assertEqual(get_instance(user_id=self.user.id), self.neighborhood)
        self.assertEqual(get_instance(username=self.user.username), self.neighborhood)
