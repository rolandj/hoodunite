from unittest import mock

from django.test import TestCase

from rest_framework.exceptions import PermissionDenied

from hoodunite.apps.location.models import Address
from hoodunite.apps.authentication.models import User
from hoodunite.apps.neighborhoods.models import Neighborhood
from hoodunite.apps.neighborhoods.services import create_neighborhood


class TestServices(TestCase):
    """Test permissions and if service behaves properly on api call."""

    def setUp(self):
        self.hood_id = '1'
        self.data = {
            'name': 'TestNeighborhood',
            'address': {
                "country": "TestCountry",
                "province": "TestProvince",
                "city": "TestCity",
                "street": "TestStreet",
                "number": 1,
                "longitude": 1,
                "latitude": 1,
            }
        }
        self.data_without_address = {
            'name': 'TestNeighborhood'
        }
        self.address = Address.objects.create(**self.data.get('address'))
        self.neighborhood = Neighborhood.objects.create(**self.data_without_address, address=self.address)
        self.user = User.objects.create(username='TestUser', password='TestPassword', neighborhood=self.neighborhood)
        self.second_user = User.objects.create(username='TestUser2', password='TestPassword')

    @mock.patch('hoodunite.apps.neighborhoods.services.Neighborhood.objects.create')
    @mock.patch('hoodunite.apps.neighborhoods.services.create_address')
    def test_create_neighborhood_with_and_without_address(self, address_create_mock, neighborhood_create_mock):
        create_neighborhood(self.second_user, **self.data)
        address_create_mock.assert_called_once_with(**self.data['address'])
        neighborhood_create_mock.assert_called_once()

        address_create_mock.reset_mock()
        neighborhood_create_mock.reset_mock()

        create_neighborhood(self.second_user, **self.data_without_address)
        address_create_mock.assert_not_called()
        neighborhood_create_mock.assert_called_once_with(**self.data_without_address)

    def test_user_can_create_only_one_neighborhood(self):
        self.assertRaises(PermissionDenied, lambda: create_neighborhood(self.user, **self.data))
