from unittest.mock import patch

from django.test import TestCase

from rest_framework.test import APIClient
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response

from hoodunite.apps.authentication.models import User


class TestNeighborhoodList(TestCase):
    """Test if api calls a service."""

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create(username='TestUser', password='TestPassword')
        self.client.force_authenticate(self.user)
        self.url = '/api/hoods/'
        self.data = {
            'name': 'TestNeighborhood',
            'address': {
                "country": "TestCountry",
                "province": "TestProvince",
                "city": "TestCity",
                "street": "TestStreet",
                "number": 1
            }
        }

    @patch.object(LimitOffsetPagination, 'paginate_queryset')
    @patch.object(LimitOffsetPagination, 'get_paginated_response')
    @patch('hoodunite.apps.neighborhoods.api.get_neighborhoods')
    def test_get_calls_selector(self, get_address_list_mock, paginator_get_response, paginator_queryset):
        paginator_get_response.return_value = Response()

        self.client.get('/api/hoods/?offset=0&limit=10')
        get_address_list_mock.assert_called_once_with(self.user, offset=['0'], limit=['10'])

    @patch.object(LimitOffsetPagination, 'paginate_queryset')
    @patch.object(LimitOffsetPagination, 'get_paginated_response')
    @patch('hoodunite.apps.neighborhoods.api.get_neighborhoods')
    def test_get_calls_selector_with_query_params(self, get_mock, paginator_get_response, paginator_queryset):
        paginator_get_response.return_value = Response()
        url = '/api/hoods/?offset=0&limit=10'
        country = self.data.get("address")["country"]
        province = self.data.get("address")["province"]
        city = self.data.get("address")["city"]

        query_by_country = f'{url}&country={country}'
        self.client.get(query_by_country)
        get_mock.assert_called_with(self.user, country=[country], offset=['0'], limit=['10'])

        query_by_province = f'{url}&province={province}'
        self.client.get(query_by_province)
        get_mock.assert_called_with(self.user, province=[province], offset=['0'], limit=['10'])

        query_by_city = f'{url}&city={city}'
        self.client.get(query_by_city)
        get_mock.assert_called_with(self.user, city=[city], offset=['0'], limit=['10'])

    @patch('hoodunite.apps.neighborhoods.api.create_neighborhood')
    def test_post_calls_service(self, create_address_mock):
        self.client.post(self.url, self.data, format='json')
        create_address_mock.assert_called_once_with(self.user, **self.data)


class TestNeighborhoodDetail(TestCase):
    """Test if api calls a service."""

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create(username='TestUser', password='TestPassword')
        self.client.force_authenticate(self.user)
        self.url = '/api/hoods/'
        self.hood_id = 1
        self.hood_name = 'TestNeighborhood'
        self.data = {
            'name': 'TestNeighborhood',
            'address': {
                "country": "TestCountry",
                "province": "TestProvince",
                "city": "TestCity",
                "street": "TestStreet",
                "number": 1
            }
        }

    @patch('hoodunite.apps.neighborhoods.api.get_neighborhood')
    def test_get_calls_selector_with_proper_kwargs(self, get_neighborhood_mock):
        self.client.get(f'{self.url}{self.hood_id}/')
        get_neighborhood_mock.assert_called_once_with(self.user, hood_id=self.hood_id)


    @patch('hoodunite.apps.neighborhoods.api.update_neighborhood')
    def test_put_calls_service_with_proper_kwargs(self, update_address_mock):
        self.client.put(f'{self.url}{self.hood_id}/', self.data, format='json')
        update_address_mock.assert_called_once_with(self.user, hood_id=self.hood_id, **self.data)


    @patch('hoodunite.apps.neighborhoods.api.patch_neighborhood')
    def test_patch_calls_service_with_proper_kwargs(self, update_address_mock):
        self.client.patch(f'{self.url}{self.hood_id}/', self.data, format='json')
        update_address_mock.assert_called_once_with(self.user, hood_id=self.hood_id, **self.data)


    @patch('hoodunite.apps.neighborhoods.api.delete_neighborhood')
    def test_delete_calls_service_with_proper_kwargs(self, delete_address_mock):
        self.client.delete(f'{self.url}{self.hood_id}/')
        delete_address_mock.assert_called_once_with(self.user, hood_id=self.hood_id)

