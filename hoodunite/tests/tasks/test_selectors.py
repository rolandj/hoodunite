from unittest import mock

from django.test import TestCase

from rest_framework.exceptions import PermissionDenied

from hoodunite.apps.tasks.models import Task
from hoodunite.apps.authentication.models import User
from hoodunite.apps.neighborhoods.models import Neighborhood
from hoodunite.apps.tasks.selectors import get_tasks, check_permissions


class TestSelectors(TestCase):
    def setUp(self):
        self.data = {
            'name': 'Test Name',
            'description': 'Lorem ipsum test doloret',
            'price': 100
        }

        self.neighborhood = Neighborhood.objects.create(name='TestNeighborhood')
        self.second_neighborhood = Neighborhood.objects.create(name='TestNeighborhood2')

        self.user = User.objects.create(username='TestUser', password='Password', neighborhood=self.neighborhood)
        self.second_user = User.objects.create(username='TestUser2', password='Password',
                                               neighborhood=self.second_neighborhood)

        self.task = Task.objects.create(**self.data, owner=self.user, neighborhood=self.neighborhood)
        self.second_task = Task.objects.create(**self.data, owner=self.second_user,
                                               neighborhood=self.second_neighborhood)

    @mock.patch('hoodunite.apps.tasks.selectors.Task.objects.filter')
    def test_task_filtering_with_query_params(self, filter_mock):
        params_owner = {'owner': [self.user.id]}
        params_owner_username = {'owner_username': [self.user.username]}
        params_neighborhood = {'neighborhood': [self.neighborhood.id]}

        get_tasks(self.user, **params_owner)
        filter_mock.assert_called_with(owner__id=self.user.id)

        get_tasks(self.user, **params_owner_username)
        filter_mock.assert_called_with(owner__username=self.user.username)

        get_tasks(self.user, **params_neighborhood)
        filter_mock.assert_called_with(neighborhood__id=self.neighborhood.id)

        # Returns user's neighborhood tasks by default
        get_tasks(self.user)
        filter_mock.assert_called_with(neighborhood__id=self.neighborhood.id)

    def test_user_can_fetch_tasks_only_from_his_neighborhood(self):
        get_tasks(self.user, **{'owner': [self.user.id]})
        get_tasks(self.second_user, **{'owner': [self.second_user.id]})
        self.assertRaises(PermissionDenied, lambda: get_tasks(self.second_user, **{'owner': [self.user.id]}))
        self.assertRaises(PermissionDenied, lambda: get_tasks(self.user, **{'owner': [self.second_user.id]}))

    def test_user_cannot_edit_finished_task(self):
        check_permissions(self.second_user, self.second_task)
        self.second_task.status = Task.FINISHED
        self.second_task.save()
        self.assertRaises(PermissionDenied, lambda: check_permissions(self.second_user, self.second_task))

    def test_user_cannot_take_already_taken_task(self):
        mandatory = User.objects.create(username='TestUser4', password='Password', neighborhood=self.neighborhood)
        other_mandatory = User.objects.create(username='TestUser3', password='Password', neighborhood=self.neighborhood)

        self.task.mandatory = mandatory
        self.task.save()
        self.assertRaises(PermissionDenied, lambda: check_permissions(other_mandatory, self.task))

    def test_user_can_take_only_ongoing_tasks(self):
        self.assertEquals(check_permissions(self.user, self.second_task), 2)  # 2 means user can become task's mandatory
        self.assertEquals(check_permissions(self.second_user, self.task), 2)

        self.task.status = Task.FINISHED
        self.second_task.status = Task.FINISHED
        self.task.save()
        self.second_task.save()

        self.assertRaises(PermissionDenied, lambda: check_permissions(self.user, self.second_task))
        self.assertRaises(PermissionDenied, lambda: check_permissions(self.second_user, self.task))
