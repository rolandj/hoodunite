from unittest.mock import patch

from django.test import TestCase

from rest_framework.test import APIClient
from rest_framework.response import Response
from rest_framework.pagination import LimitOffsetPagination

from hoodunite.apps.tasks.models import Task
from hoodunite.apps.authentication.models import User
from hoodunite.apps.neighborhoods.models import Neighborhood


class TestTaskList(TestCase):
    def setUp(self):
        self.data = {
            'name': 'Test Name',
            'description': 'Lorem ipsum test doloret',
            'price': 100
        }
        self.client = APIClient()
        self.url = '/api/tasks/'
        self.neighborhood = Neighborhood.objects.create(name='TestNeighborhood')
        self.user = User.objects.create(username='TestUser', password='Password')
        self.other_user = User.objects.create(username='TestUser2', password='Password')
        self.client.force_authenticate(self.user)

    @patch.object(LimitOffsetPagination, 'paginate_queryset')
    @patch.object(LimitOffsetPagination, 'get_paginated_response')
    @patch('hoodunite.apps.tasks.api.get_tasks')
    def test_get_calls_selector(self, get_neighborhood_mock, paginator_response, paginator_queryset):
        paginator_response.return_value = Response()

        self.client.get('/api/tasks/?offset=0&limit=10')
        get_neighborhood_mock.assert_called_once_with(self.user, offset=['0'], limit=['10'])

    @patch.object(LimitOffsetPagination, 'paginate_queryset')
    @patch.object(LimitOffsetPagination, 'get_paginated_response')
    @patch('hoodunite.apps.tasks.api.get_tasks')
    def test_get_calls_selector_with_query_params(self, get_neighborhood_mock, paginator_response, paginator_queryset):
        paginator_response.return_value = Response()
        url = '/api/tasks/?offset=0&limit=10'

        query_by_owner = f'{url}&owner={self.user.id}'
        self.client.get(query_by_owner)
        get_neighborhood_mock.assert_called_with(self.user, owner=[f'{self.user.id}'], offset=['0'], limit=['10'])

        query_by_neighborhood = f'{url}&neighborhood={self.neighborhood.id}'
        self.client.get(query_by_neighborhood)
        get_neighborhood_mock.assert_called_with(self.user, neighborhood=[f'{self.neighborhood.id}'], offset=['0'],
                                                 limit=['10'])

    @patch('hoodunite.apps.tasks.api.create_task')
    def test_post_calls_service(self, create_mock):
        self.client.post(self.url, data=self.data)
        create_mock.assert_called_once_with(self.user, **self.data)


class TestTaskDetail(TestCase):
    def setUp(self):
        self.data = {
            'name': 'Test Name',
            'description': 'Lorem ipsum test doloret',
            'price': 100
        }
        self.user = User.objects.create(username='TestUser', password='Password')
        self.neighborhood = Neighborhood.objects.create(name='TestNeighborhood')
        self.task = Task.objects.create(**self.data, owner=self.user, neighborhood=self.neighborhood)
        self.other_user = User.objects.create(username='TestUser2', password='Password')
        self.client = APIClient()
        self.client.force_authenticate(self.user)
        self.url = f'/api/tasks/{self.task.id}/'

    @patch('hoodunite.apps.tasks.api.get_task')
    def test_get_calls_selector(self, get_mock):
        get_mock.return_value = self.task

        self.client.get(self.url)
        get_mock.assert_called_once_with(self.user, task_id=self.task.id)

    @patch('hoodunite.apps.tasks.api.update_task')
    def test_put_calls_service(self, update_mock):
        self.client.put(self.url, data=self.data)
        update_mock.assert_called_once_with(self.user, **self.data)

    @patch('hoodunite.apps.tasks.api.patch_task')
    def test_patch_calls_service(self, patch_mock):
        self.client.patch(self.url, data={'price': 101})
        patch_mock.assert_called_once_with(self.user, price=101)

    @patch('hoodunite.apps.tasks.api.delete_task')
    def test_delete_calls_service(self, delete_mock):
        self.client.delete(self.url)
        delete_mock.assert_called_once_with(self.user)
