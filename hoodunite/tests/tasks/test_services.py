from unittest import mock

from django.test import TestCase

from hoodunite.apps.tasks.models import Task
from hoodunite.apps.tasks.services import patch_task
from hoodunite.apps.authentication.models import User
from hoodunite.apps.neighborhoods.models import Neighborhood


class TestServices(TestCase):
    def setUp(self):
        self.neighborhood = Neighborhood.objects.create(name='TestNeighborhood')
        self.other_hood = Neighborhood.objects.create(name='TestNeighborhood2')
        self.user = User.objects.create(username='TestUser', password='Password', neighborhood=self.neighborhood)
        self.second_user = User.objects.create(username='TestUser2', password='Password',
                                               neighborhood=self.neighborhood)
        self.user_mandatory = User.objects.create(username='TestUser3', password='Password',
                                                  neighborhood=self.other_hood)
        self.create_data = {
            'name': 'Test Name',
            'description': 'Lorem ipsum test doloret',
            'price': 100
        }
        self.task = Task.objects.create(**self.create_data, owner=self.user, neighborhood=self.neighborhood)
        self.other_task = Task.objects.create(**self.create_data, owner=self.user_mandatory,
                                              neighborhood=self.other_hood)

    @mock.patch('hoodunite.apps.tasks.services.Task.objects.filter')
    def test_user_takes_task(self, task_filter_mock):
        update_mock = mock.MagicMock()
        task_filter_mock(mandatory=self.user_mandatory).update = update_mock

        patch_task(self.user_mandatory, task_id=self.task.id, mandatory_id=self.user_mandatory.id)
        update_mock.assert_called_once_with(mandatory=self.user_mandatory)
