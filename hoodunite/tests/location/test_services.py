from unittest import mock

from django.test import TestCase

from rest_framework.exceptions import NotFound
from geopy import Nominatim

from hoodunite.apps.authentication.models import User
from hoodunite.apps.location.models import Address
from hoodunite.apps.neighborhoods.models import Neighborhood
from hoodunite.apps.location.services import patch_address, __get_coordinates as get_coordinates


class TestServices(TestCase):
    def setUp(self):
        self.url = '/api/addresses/'
        self.data = {
            "country": "USA",
            "province": "New York",
            "city": "New York",
            "street": "5th Avenue",
            "number": 1
        }
        self.address = Address.objects.create(**self.data, latitude=1, longitude=1)
        self.user = User.objects.create(username='TestUser', password='TestPassword', address=self.address)
        self.neighborhood = Neighborhood.objects.create(name='TestNeighborhood', address=self.address)

    def test_address_created_with_coordinates(self):
        with mock.patch.object(Nominatim, 'geocode') as geolocator_mock:
            get_coordinates(**self.data)
            geolocator_mock.assert_called_once_with(str(self.address))

    @mock.patch.object(Nominatim, 'geocode')
    def test_patch_neighborhood_address(self, geolocator_mock):
        geolocator_mock.return_value.latitude = 1
        geolocator_mock.return_value.longitude = 1

        self.data['country'] = 'Poland'
        self.data['province'] = 'wielkopolskie'
        self.data['city'] = 'Poznan'
        self.data['street'] = 'Polwiejska'
        self.data['number'] = 2
        patch_address(self.neighborhood, **self.data)

    @mock.patch.object(Nominatim, 'geocode')
    def test_patch_user_address(self, geolocator_mock):
        geolocator_mock.return_value.latitude = 1
        geolocator_mock.return_value.longitude = 1

        self.data['country'] = 'Poland'
        self.data['province'] = 'wielkopolskie'
        self.data['city'] = 'Poznan'
        self.data['street'] = 'Polwiejska'
        self.data['number'] = 2
        patch_address(self.user, **self.data)

    def test_patch_address_raises_not_found_when_no_owner(self):
        self.assertRaises(NotFound, lambda: patch_address(user=None, neighborhood=None, **self.data))
