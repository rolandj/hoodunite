from django.contrib import admin
from django.urls import path, include

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from hoodunite.apps.neighborhoods.urls import urlpatterns as hood_urls
from hoodunite.apps.authentication.urls import urlpatterns as user_urls
from hoodunite.apps.tasks.urls import urlpatterns as task_urls

from drf_yasg import openapi
from rest_framework import permissions
from drf_yasg.views import get_schema_view

schema_view = get_schema_view(
    openapi.Info(
        title="Hoodunite API",
        default_version='0.0',
        description="Hoodunite API endpoints.",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),

    path('api/hoods/', include(hood_urls)),
    path('api/users/', include(user_urls)),
    path('api/tasks/', include(task_urls)),

    path('api/tokens/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/tokens/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    path('api/swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('api/redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
