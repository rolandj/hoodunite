from django.apps import AppConfig


class NeighborhoodsConfig(AppConfig):
    name = 'hoodunite.apps.neighborhoods'
    label = 'hoodunite_apps_neighborhoods'
