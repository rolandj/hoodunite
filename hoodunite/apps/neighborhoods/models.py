from django.db import models
from hoodunite.apps.location.models import Address


class Neighborhood(models.Model):
    name = models.CharField(max_length=30, unique=True)
    address = models.OneToOneField(Address, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name
