from django.urls import path

from .api import NeighborhoodList
from .api import NeighborhoodDetail

urlpatterns = [
    path('', NeighborhoodList.as_view()),
    path('<int:hood_id>/', NeighborhoodDetail.as_view()),
]
