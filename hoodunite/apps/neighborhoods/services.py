from rest_framework.exceptions import PermissionDenied

from .models import Neighborhood
from .selectors import get_instance, check_permissions
from hoodunite.apps.location.services import create_address, patch_address


def create_neighborhood(user, name=None, address=None):
    if not user.neighborhood:
        if address:
            location = create_address(**address)
            Neighborhood.objects.create(name=name, address=location)
        else:
            Neighborhood.objects.create(name=name)
    else:
        raise PermissionDenied


def update_neighborhood(user, hood_name=None, hood_id=None, user_id=None, **data):
    instance = get_instance(hood_name=hood_name, hood_id=hood_id, user_id=user_id)
    check_permissions(user, instance)

    name, location_data = data.get('name'), data.get('address')
    address, _ = patch_address(hood_id=instance.id, **location_data)
    Neighborhood.objects.update_or_create(id=instance.id, defaults={'name': name, 'address': address})


def patch_neighborhood(user, hood_name=None, hood_id=None, user_id=None, **data):
    instance = get_instance(hood_name=hood_name, hood_id=hood_id, user_id=user_id)
    check_permissions(user, instance)

    name, location = data.get('name'), data.get('address')
    if location:
        patch_address(hood_id=instance.id, **location)
    if name:
        Neighborhood.objects.filter(id=instance.id).update(name=name)


def delete_neighborhood(user, **kwargs):
    instance = get_instance(**kwargs)
    check_permissions(user, instance)

    instance.delete()
