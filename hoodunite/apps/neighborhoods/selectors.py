from django.shortcuts import get_object_or_404

from rest_framework.exceptions import NotFound, PermissionDenied

from .models import Neighborhood
from hoodunite.apps.authentication.models import User


def get_neighborhood(user, **kwargs):
    instance = get_instance(**kwargs)
    if check_permissions(user, instance):
        return instance


def get_neighborhoods(user, **query_params):
    country = query_params.get('country', None)
    province = query_params.get('province', None)
    city = query_params.get('city', None)

    if city:
        return Neighborhood.objects.filter(address__city=city[0])
    elif province:
        return Neighborhood.objects.filter(address__province=province[0])
    elif country:
        return Neighborhood.objects.filter(address__country=country[0])
    else:
        return Neighborhood.objects.filter(id=user.neighborhood.id)


def get_instance(hood_name=None, hood_id=None, user_id=None, username=None):
    """Return Neighborhood instance, also in nested url."""

    if user_id:
        return get_object_or_404(User, id=user_id).neighborhood
    elif username:
        return get_object_or_404(User, username=username).neighborhood
    elif hood_id:
        return get_object_or_404(Neighborhood, id=hood_id)
    elif hood_name:
        return get_object_or_404(Neighborhood, name=hood_name)
    else:
        raise NotFound()


def check_permissions(user, instance):
    """Check if user has permissions to edit instance.

    Can edit instance only when there is only one User bound to given Neighborhood."""
    if user.neighborhood.id == instance.id and len(User.objects.filter(neighborhood=instance)) == 1:
        return True
    else:
        raise PermissionDenied()
