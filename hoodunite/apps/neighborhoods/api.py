from rest_framework import serializers
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.views import APIView, Response, status

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from .models import Neighborhood
from .selectors import get_neighborhood, get_neighborhoods
from .services import create_neighborhood, update_neighborhood, patch_neighborhood, delete_neighborhood

from hoodunite.apps.location.serializers import AddressSerializer


class NeighborhoodSerializer(serializers.ModelSerializer):
    address = AddressSerializer()

    class Meta:
        model = Neighborhood
        fields = ('id', 'name', 'address')


class NeighborhoodList(APIView, LimitOffsetPagination):
    # Manually define query parameters for swagger
    p_country = openapi.Parameter('country', openapi.IN_QUERY, type=openapi.TYPE_STRING)
    p_province = openapi.Parameter('province', openapi.IN_QUERY, description='Filter by province',
                                   type=openapi.TYPE_STRING)
    p_city = openapi.Parameter('city', openapi.IN_QUERY, description='Filter by city', type=openapi.TYPE_STRING)
    p_offset = openapi.Parameter('offset', openapi.IN_QUERY, description='Select offset', type=openapi.TYPE_INTEGER)
    p_limit = openapi.Parameter('limit', openapi.IN_QUERY, description='Select limit', type=openapi.TYPE_INTEGER)

    @swagger_auto_schema(manual_parameters=[p_country, p_province, p_city, p_offset, p_limit])
    def get(self, request, *args, **kwargs):
        neighborhoods = get_neighborhoods(request.user, **request.query_params)
        paginated_data = self.paginate_queryset(neighborhoods, request, view=self)
        data = NeighborhoodSerializer(paginated_data, many=True).data

        return self.get_paginated_response(data)

    def post(self, request):
        serializer = NeighborhoodSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        create_neighborhood(request.user, **serializer.validated_data)

        return Response(serializer.validated_data, status=status.HTTP_201_CREATED)


class NeighborhoodDetail(APIView):
    def get(self, request, *args, **kwargs):
        hood = get_neighborhood(request.user, **kwargs)
        serializer = NeighborhoodSerializer(hood)

        return Response(serializer.data)

    def put(self, request, *args, **kwargs):
        serializer = NeighborhoodSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        update_neighborhood(request.user, **kwargs, **serializer.validated_data)

        return Response(serializer.validated_data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        serializer = NeighborhoodSerializer(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        patch_neighborhood(request.user, **kwargs, **serializer.validated_data)

        return Response(serializer.validated_data, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        delete_neighborhood(request.user, **kwargs)

        return Response(status=status.HTTP_204_NO_CONTENT)
