from rest_framework.exceptions import NotFound


def get_instance(user=None, neighborhood=None):
    if user:
        return user.address
    elif neighborhood:
        return neighborhood.address
    else:
        raise NotFound()
