from rest_framework.exceptions import MethodNotAllowed, Throttled

from geopy.geocoders import Nominatim
from geopy.exc import GeocoderServiceError

from .models import Address
from .selectors import get_instance


def create_address(**data):
    coordinates = __get_coordinates(**data)
    return Address.objects.create(**data, latitude=coordinates.latitude, longitude=coordinates.longitude)


def patch_address(user=None, neighborhood=None, **data):
    instance = get_instance(user, neighborhood)

    country = data.get('country', instance.country)
    province = data.get('province', instance.province)
    city = data.get('city', instance.city)
    street = data.get('street', instance.street)
    number = data.get('number', instance.number)

    if country != instance.country:
        Address.objects.filter(id=instance.id).update(country=country)
    if province != instance.country:
        Address.objects.filter(id=instance.id).update(province=province)
    if city != instance.country:
        Address.objects.filter(id=instance.id).update(city=city)
    if street != instance.country:
        Address.objects.filter(id=instance.id).update(street=street)
    if number != instance.country:
        Address.objects.filter(id=instance.id).update(number=number)

    coordinates = __get_coordinates(country, province, city, street, number)
    Address.objects.filter(id=instance.id).update(latitude=coordinates.latitude, longitude=coordinates.longitude)


def delete_address(user=None, neighborhood=None):
    instance = get_instance(user, neighborhood)
    instance.delete()


def __get_coordinates(country, province, city, street, number):
    geolocator = Nominatim(user_agent='hoodunite')
    try:
        coordinates = geolocator.geocode(f'{number} {street} {city} {province} {country}')
    except GeocoderServiceError:
        raise Throttled(wait=1)

    if coordinates is not None:
        return coordinates

    raise MethodNotAllowed(method='find coordinates')
