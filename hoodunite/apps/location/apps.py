from django.apps import AppConfig


class LocationConfig(AppConfig):
    name = 'hoodunite.apps.location'
    label = 'hoodunite_apps_location'
