from django.db import models


class Address(models.Model):
    country = models.CharField(max_length=40)
    province = models.CharField(max_length=40)
    city = models.CharField(max_length=40)
    street = models.CharField(max_length=40)
    number = models.PositiveIntegerField()

    latitude = models.FloatField()
    longitude = models.FloatField()

    def __str__(self):
        return f'{self.number} {self.street} {self.city} {self.province} {self.country}'
