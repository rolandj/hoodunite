# Generated by Django 3.0.3 on 2020-03-02 11:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hoodunite_apps_authentication', '0002_auto_20200302_1156'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='date_of_birth',
            field=models.DateField(blank=True, null=True),
        ),
    ]
