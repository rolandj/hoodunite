from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from .services import (
    create_user,
    update_user,
    patch_user,
    delete_user,
)
from .selectors import (
    get_user,
    get_users,
)
from .serializers import (
    UserSerializer,
    UserSerializerWithToken,
)


class UserList(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        users = get_users(request.user)
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = UserSerializerWithToken(data=request.data)
        serializer.is_valid(raise_exception=True)
        create_user(**serializer.validated_data)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class UserDetail(APIView):
    def get(self, request, user_id):
        user = get_user(request.user, user_id)
        serializer = UserSerializer(user)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, user_id):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        update_user(request.user, user_id, **serializer.validated_data)
        return Response(serializer.validated_data, status=status.HTTP_201_CREATED)

    def patch(self, request, user_id):
        serializer = UserSerializer(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        patch_user(request.user, user_id, **serializer.validated_data)
        return Response(serializer.validated_data, status=status.HTTP_201_CREATED)

    def delete(self, request, user_id):
        delete_user(request.user, user_id)

        return Response(status=status.HTTP_204_NO_CONTENT)
