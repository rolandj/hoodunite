import datetime

from django.db import models
from django.contrib.auth.models import AbstractUser

from hoodunite.apps.neighborhoods.models import Neighborhood
from hoodunite.apps.location.models import Address


class User(AbstractUser):
    MAN = 'M'
    WOMAN = 'W'
    GENDER_CHOICES = [
        (MAN, 'Man'),
        (WOMAN, 'Woman')
    ]

    email = models.EmailField(unique=True)
    bio = models.TextField(null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.CharField(max_length=2, choices=GENDER_CHOICES, null=True, blank=True)
    neighborhood = models.ForeignKey(Neighborhood, on_delete=models.CASCADE, null=True, blank=True)
    address = models.OneToOneField(Address, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f'{self.username}'

    @property
    def is_adult(self):
        """ Check if user is adult """
        today = datetime.datetime.today()
        age = today.year - self.date_of_birth.year - (
                (today.month, today.day) < (self.date_of_birth.month, self.date_of_birth.day))
        return age >= 18
