from rest_framework import serializers

from .models import User
from .services import generate_tokens


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name',
                  'password', 'email', 'bio', 'date_of_birth',
                  'gender', 'address', 'neighborhood']
        extra_kwargs = {'password': {'write_only': True}}


class UserSerializerWithToken(serializers.ModelSerializer):
    tokens = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name',
                  'password', 'email', 'bio', 'date_of_birth',
                  'gender', 'address', 'neighborhood', 'tokens')
        extra_kwargs = {'password': {'write_only': True}}

    def get_tokens(self, request_data):
        username = self.validated_data.get('username')
        user = User.objects.get(username=username)
        return generate_tokens(user)
