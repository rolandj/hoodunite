from rest_framework_simplejwt.tokens import RefreshToken

from .models import User
from .selectors import get_user, check_permissions


def create_user(**kwargs):
    user = User.objects.create_user(**kwargs)
    user.set_password(kwargs.get('password'))
    user.save()
    return user


def update_user(user, user_id, **kwargs):
    check_permissions(user, user_id)
    User.objects.update_or_create(id=user_id, defaults={**kwargs})


def patch_user(user, user_id, **kwargs):
    check_permissions(user, user_id)
    User.objects.filter(id=user_id).update(**kwargs)


def delete_user(user, user_id):
    check_permissions(user, user_id)
    user = get_user(user, user_id)
    user.delete()


def generate_tokens(user) -> dict:
    """Return dictionary with access and refresh tokens."""

    tokens = RefreshToken.for_user(user)
    refresh = str(tokens)
    access = str(tokens.access_token)
    return {
        "refresh": refresh,
        "access": access
    }
