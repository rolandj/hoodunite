from . import api
from django.urls import path


urlpatterns = [
    path('', api.UserList.as_view()),
    path('<int:user_id>/', api.UserDetail.as_view(), name='user_detail'),
]
