from django.shortcuts import get_object_or_404

from rest_framework.exceptions import PermissionDenied

from .models import User


def get_user(user, user_id):
    requested_user = get_object_or_404(User, id=user_id)
    check_permissions(user, user_id, basic=True, requested_user=requested_user)

    return requested_user


def get_users(user):
    return User.objects.filter(neighborhood__id=user.neighborhood.id)


def check_permissions(logged_user, requested_user_id, basic=None, requested_user=None):
    """
    Check if requested user is currently logged in.
    @:param basic Check if logged user belongs to the same Neighborhood as requested user
    @:param requested_user Requested instance, obligatory on basic check
    """

    if basic:
        if logged_user.neighborhood.id == requested_user.neighborhood.id:
            return True
    elif logged_user.id == requested_user_id:
        return True
    raise PermissionDenied
