from django.apps import AppConfig


class AuthenticationConfig(AppConfig):
    name = 'hoodunite.apps.authentication'
    label = 'hoodunite_apps_authentication'
