from django.shortcuts import get_object_or_404

from rest_framework.exceptions import PermissionDenied

from .models import Task


def get_task(user, task_id):
    instance = get_instance(task_id)
    check_permissions(user, instance, basic=True)
    return instance


def get_tasks(user, **query_params):
    """Filter tasks.

    User could only request tasks bound to his neighborhood."""

    owner = query_params.get('owner', None)
    owner_username = query_params.get('owner_username', None)
    hood_id = query_params.get('neighborhood', None)

    if owner:
        tasks = Task.objects.filter(owner__id=owner[0])
    elif owner_username:
        tasks = Task.objects.filter(owner__username=owner_username[0])
    elif hood_id:
        tasks = Task.objects.filter(neighborhood__id=hood_id[0])
    else:
        tasks = Task.objects.filter(neighborhood__id=user.neighborhood.id)

    for task in tasks:
        check_permissions(user, task, basic=True)
    return tasks


def check_permissions(user, instance, delete=None, basic=None):
    """Check if user has permissions to instance.

    Other user may declare to take on task and change mandatory field.
    :param delete Check permission to delete instance
    :param basic Check if instances neighborhood is authenticated user's neighborhood
    :return True or False or int(2) when user requests being task mandatory."""

    if basic:
        if user.neighborhood.id == instance.neighborhood.id:
            return True
        else:
            raise PermissionDenied(detail='Cannot fetch tasks from other Neighborhood.')
    elif instance.owner == user and delete:
        return True
    elif instance.owner == user and instance.status != Task.FINISHED:
        return True
    elif instance.owner != user and not instance.mandatory and instance.status != Task.FINISHED:
        return 2

    raise PermissionDenied


def get_instance(task_id):
    return get_object_or_404(Task, id=task_id)
