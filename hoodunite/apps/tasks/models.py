from django.db import models

from hoodunite.apps.authentication.models import User
from hoodunite.apps.neighborhoods.models import Neighborhood


class Task(models.Model):
    OPEN = 'O'
    FINISHED = 'F'
    ONGOING = 'OG'
    STATUS_CHOICES = [
        (OPEN, 'Open'),
        (FINISHED, 'Finished'),
        (ONGOING, 'Ongoing'),
    ]

    name = models.CharField(max_length=30)
    description = models.TextField(max_length=300)
    date = models.DateTimeField(auto_now=True)
    price = models.PositiveIntegerField()
    status = models.CharField(max_length=2, choices=STATUS_CHOICES, default=OPEN)

    neighborhood = models.ForeignKey(Neighborhood, on_delete=models.CASCADE)
    mandatory = models.OneToOneField(User, related_name='tasks_mandatory', on_delete=models.CASCADE, null=True,
                                     blank=True)
    owner = models.OneToOneField(User, related_name='tasks_owner', on_delete=models.CASCADE)
