from django.shortcuts import get_object_or_404

from hoodunite.apps.authentication.models import User
from hoodunite.apps.neighborhoods.models import Neighborhood

from .models import Task
from .selectors import check_permissions, get_instance


def create_task(user, name, description, price):
    hood = get_object_or_404(Neighborhood, user.neighborhood.id)
    Task.objects.create(user, name=name, description=description, price=price, owner=user, neighborhood=hood)


def update_task(user, task_id, description=None, price=None, status=None):
    instance = get_instance(task_id)
    check_permissions(user, instance)

    Task.objects.update_or_create(defaults={'description': description, 'price': price, 'status': status},
                                  id=instance.id)


def patch_task(user, task_id, description=None, price=None, status=None, mandatory_id=None):
    instance = get_instance(task_id)

    permission_status = check_permissions(user, instance)

    if description and not permission_status == 2:
        Task.objects.filter(id=instance.id).update(description=description)
    if price and not permission_status == 2:
        Task.objects.filter(id=instance.id).update(price=price)
    if status and not permission_status == 2:
        Task.objects.filter(id=instance.id).update(status=status)
    if mandatory_id and permission_status == 2:
        mandatory = User.objects.get(id=mandatory_id)
        Task.objects.filter(id=instance.id).update(mandatory=mandatory)


def delete_task(user, task_id):
    instance = get_instance(task_id)
    check_permissions(user, instance, delete=True)

    instance.delete()
