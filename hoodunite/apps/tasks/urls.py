from django.urls import path

from .api import TaskListView
from .api import TaskDetailView

urlpatterns = [
    path('', TaskListView.as_view()),
    path('<int:task_id>/', TaskDetailView.as_view()),
]
