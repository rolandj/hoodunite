from django.apps import AppConfig


class TasksConfig(AppConfig):
    name = 'hoodunite.apps.tasks'
    label = 'hoodunite_apps_tasks'
