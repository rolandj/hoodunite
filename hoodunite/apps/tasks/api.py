from rest_framework.serializers import ModelSerializer
from rest_framework.views import APIView, Response, status
from rest_framework.pagination import LimitOffsetPagination

from hoodunite.apps.neighborhoods.api import NeighborhoodSerializer
from hoodunite.apps.authentication.api import UserSerializer

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from .models import Task
from .selectors import get_task, get_tasks
from .services import create_task, update_task, patch_task, delete_task


class TaskSerializer(ModelSerializer):
    neighborhood = NeighborhoodSerializer()
    owner = UserSerializer()
    mandatory = UserSerializer()

    class Meta:
        model = Task
        fields = ('id', 'name', 'description', 'date', 'price', 'status', 'neighborhood', 'owner', 'mandatory')


class TaskListView(APIView, LimitOffsetPagination):
    # Manually define query parameters for swagger
    p_offset = openapi.Parameter('offset', openapi.IN_QUERY, description='Select offset', type=openapi.TYPE_INTEGER)
    p_limit = openapi.Parameter('limit', openapi.IN_QUERY, description='Select limit', type=openapi.TYPE_INTEGER)

    @swagger_auto_schema(manual_parameters=[p_offset, p_limit])
    def get(self, request, *args, **kwargs):
        tasks = get_tasks(request.user, **request.query_params)
        paginated_data = self.paginate_queryset(tasks, request, view=self)
        data = TaskSerializer(paginated_data, many=True).data

        return self.get_paginated_response(data)

    def post(self, request, *args, **kwargs):
        serializer = TaskSerializer(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        create_task(request.user, **serializer.validated_data)
        return Response(serializer.validated_data, status=status.HTTP_201_CREATED)


class TaskDetailView(APIView):
    def get(self, request, *args, **kwargs):
        instance = get_task(request.user, **kwargs)
        serializer = TaskSerializer(instance)

        return Response(serializer.data)

    def put(self, request, *args, **kwargs):
        serializer = TaskSerializer(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        update_task(request.user, **serializer.validated_data)
        return Response(serializer.validated_data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        serializer = TaskSerializer(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        patch_task(request.user, **serializer.validated_data)
        return Response(serializer.validated_data, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        delete_task(request.user, **request.query_params)
        return Response(status=status.HTTP_204_NO_CONTENT)
