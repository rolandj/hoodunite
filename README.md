# hoodunite.com

## Concept
Hoodunite will be a notice board and social network to integrate
local communities. Currently project has tested, restful api with 
all necessary endpoints.
## Install
#### 1. Clone repository <br><br>
`$: git clone https://gitlab.com/rolandj/hoodunite.git` <br><br>
#### 2. Make sure you have docker-compose installed <br><br>
`$: docker-compose --version` <br><br>
#### 3. Run app locally <br><br>
`$: docker-compose up`
